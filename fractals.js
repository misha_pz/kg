

// sh(z)
const canvas_1 = document.getElementById("fr1_canvas");
const ctx = canvas_1.getContext("2d");
const { Complex } = window;
const config = {
    fr1_iterationsCount: 6,
    fr1_colorShemes: {
        first: "rgb(0, 0, 120, 1)",
        second: "rgb(0, 0, ",
    },
    fr1_scaleX: 1
};


let ii = 0
class ComplexNumber {
    constructor(r, i) {
        this.r = r;
        this.i = i;
        //this.c = Complex.from(r, i)
    }

    mag() {
        // return Math.hypot(this.c.real, this.c.imag)
        // console.log("hypot -> ", Math.hypot(this.r, this.i))
        return Math.hypot(this.r, this.i);
    }

    add(nmb) {
        this.r += nmb.r;
        this.i += nmb.i;

        //this.c.add(nmb.c)
        return this;
    }
    
    
    square () {
          //this.c = Complex.sqrt(this.c)
          let r = this.r;
          let i = this.i;
          this.r = (r * r) - (i * i);
          this.i = 2 * r * i;
          return this;
		}

    sh() {
        //this.c = Complex.sinh(this.c)
        const r = this.r;
        const i = this.i;
        const expR = Math.exp(r);
        const expNegR = Math.exp(-r);

        let complex = Complex.sinh(Complex.from(this.r, this.i))
        // this.r = Math.sinh(this.r) * Math.cos(this.i),
        // this.i = Math.cosh(this.r) * Math.sin(this.i)
        this.r = complex.real;
        this.i = complex.imag;
        // this.r = (expR - expNegR) / 2;
        // this.i = Math.sin(this.i);

        return this;
    }
}

const drawFractal = function () {
    const width = canvas_1.width * config.fr1_scaleX;
    const height = canvas_1.height * config.fr1_scaleX;
    ctx.translate(0, -(width - height) / 2);
    for (let x = 0; x < width; x++) {
        for (let y = 0; y < height; y++) {
            const a = (x - (width / 2)) / (width / 5);
            const b = (y - (height / 2)) / (width / 5);

            let z = new ComplexNumber(a, b);
            const c = new ComplexNumber(a, b);

            let color = config.fr1_colorShemes.first;
            for (let i = 0; i < config.fr1_iterationsCount; i++) {
                z.sh();

                if (z.mag() > 100) {
                    color = config.fr1_colorShemes.second + ((225 * (i / config.fr1_iterationsCount)) + 30) + ", 1)";
                    break;
                }
            }
            ctx.fillStyle = color;
            ctx.fillRect(x, y, 1, 1);
        }
    }
    console.log("finish")
};


$("#fr1_amountOfIteration").on("change", function(event){
    const value = $("#fr1_amountOfIteration").val()
    if (value <= 350){
        config.fr1_iterationsCount = value
        drawFractal();
    }else{
        alert("max is 30 iterations")
    }
})


$("#fr1_colorSheme").on("change", function(event){
    const select = $(event.target)
    const selectedId = select.find(":selected").val()
    if (selectedId === "1"){
        config.fr1_colorShemes.first = "rgb(0, 0, 120, 1)"
        config.fr1_colorShemes.second = "rgb(0, 0, "
    }else if(selectedId === "2"){
        config.fr1_colorShemes.first = "rgb(228, 0, 0, 1)"
        config.fr1_colorShemes.second = "rgb(215, 199, "
    }else if(selectedId === "3"){
        config.fr1_colorShemes.first = "rgb(215, 215, 0, 1)"
        config.fr1_colorShemes.second = "rgb(0, 0, "
    }
    drawFractal();
})

$("#fr1_scaleX").on("input", function(event){
    $(".fr1_scaleText").text("loading...")
    setTimeout(()=>{
        config.fr1_scaleX = parseFloat($(event.target).val());
        drawFractal();
        $(".fr1_scaleText").text(config.fr1_scaleX)
    }, 1000)
})




// Лінія ЧЕЗАРО

const cf_width = 500
const cf_height = 500

var cf_svg = d3.select("#cesaro-vis")
    .append("svg")
    .attr("width", cf_width)
    .attr("height", cf_height);

var cf_data = [{ x: 0, y: 0 }, { x: cf_width, y: 0 }];
var cf_check = 0;


function cesaroDraw(data) {
    d3.select("#cesaro-vis").selectAll("path").remove();

    var x = d3.scale.linear().domain([0, cf_width]).range([0, cf_width]);   
    var y = d3.scale.linear().domain([0, cf_height]).range([cf_height-10, -10]);

    var line = d3.svg.line()
        .x(function(d, i) { return x(d.x); })
        .y(function(d, i) { return y(d.y); })
        .interpolate("linear");

    var path = cf_svg.append("path")
        .attr("d", line(data) )
        .attr("stroke", "#ffffff")
        .attr("stroke-width", 2)
        .attr("fill", "none");
}


function cesaroSubdivide(numTimes) {
    var pts = cf_data;
    
    for (j = 0; j < numTimes; j++) {
        for (i = pts.length-1; i > 0; i--) {

            // 19/36 і 17/36 - точки визначені теоритично для розрізів
            const const_1 = 19/36
            const const_2 = 17/36

            // Рахується дві нові точки: p2 і p4. 
            // Ці точки розташовані на відрізку під кутом 90 градусів 
            // одна до одної і відстань між ними обчислюється наступним чином
            var p2 = { x: const_1 * pts[i-1].x + const_2 * pts[i].x, y: const_1 * pts[i-1].y + const_2 * pts[i].y };
            var p4 = { x: const_2 * pts[i-1].x + const_1 * pts[i].x, y: const_2 * pts[i-1].y + const_1 * pts[i].y };

            // Розраховується точка p3, яка розташована посередині 
            // між p2 і p4 з додатковим зміщенням, щоб створити різанину
            var s = 6 * Math.sqrt(2);
            var p3x = p2.x + (p4.x - p2.x) / 2 + s * (p2.y - p4.y);
            var p3y = p2.y + (p4.y - p2.y) / 2 + s * (p4.x - p2.x);
            
            var p3 = { x: p3x, y: p3y };
            
            pts.splice(i, 0, p2, p3, p4);

        }
    }
    
    return pts;
    
}


$("#cesaro-slider").on("change mousemove", function() {
    var cf_value = $(this).val();
    $('#cesaro-output').text(cf_value);
    
    if (cf_value != cf_check) {
        cf_check = cf_value;
        cf_data = [{ x: 0, y: 0 }, { x: cf_width, y: 0 }];
        cf_newData = cesaroSubdivide(cf_value);
        cesaroDraw(cf_newData);
    }

});



// Else:
$(".fractal-radio").on("click", function(){
    const radio = $(this)
    const fractal_id = radio.parent().data("fractal_id")
    $(".fractal-view").css("display", "none")
    $(`.fractal-view[data-fractal_id="${fractal_id}"]`).css("display", "block")
})


$(".download-button").on("click", function(){
    const radio = $("input[type='radio']:checked")
    const fractal_id = radio.parent().data("fractal_id")

    if (fractal_id === 1){
        const dataURL = document.getElementById("fr1_canvas").toDataURL("image/png");
        var a = document.createElement('a');
        a.href = dataURL
        a.download = 'fractal-sh(z).jpeg';
        a.click();
    }else if (fractal_id === 2){
        const svg = document.getElementsByTagName("svg")[0];
        svg.setAttribute("xmlns", "http://www.w3.org/2000/svg");
        var svgData = svg.outerHTML;
        var preface = '<?xml version="1.0" standalone="no"?>\r\n';
        var svgBlob = new Blob([preface, svgData], {type:"image/svg+xml;charset=utf-8"});
        var svgUrl = URL.createObjectURL(svgBlob);
        var downloadLink = document.createElement("a");
        downloadLink.href = svgUrl;
        downloadLink.download = "fratal-cesaro.svg";
        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
    }
})




window.onload = ()=>{
    drawFractal();
    document.getElementById("cesaro-vis").getElementsByTagName("svg")[0].remove()
    

    // Лінія чезаро
    cf_data = [{ x: 0, y: 0 }, { x: cf_width, y: 0 }];
    cf_newData = cesaroSubdivide($("#cesaro-slider").val());
    cesaroDraw(cf_newData);
};