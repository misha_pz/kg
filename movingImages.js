let dragStart = null;
let offsetX = 0;
let offsetY = 0;
let scale = 1;
var isBuildClicked = false;
let isStartClicked = false;
var isStopClicked = false;
var parallelogram;
var tempParallelogram;

const canvas = document.getElementById('moving-img');
const ctx = canvas.getContext('2d');
const width = canvas.width;
const height = canvas.height;
const unit = 25;
const maxUnit = 250;

let tempTrianglePoints = []
let trianglePoints = []

// Calculate initial offsets to center the grid
offsetX = width / 2;
offsetY = height / 2;

function updateMaxOffset(scale) {
    const step = unit + (maxUnit - unit) * ((scale) / (100 - 1));
    const maxOffsetX = step * 100 * (scale); // max offset at scale 100
    const maxOffsetY = step * 100 * (scale); // max offset at scale 100
    return { maxOffsetX, maxOffsetY };
}

function calculateUnitScale(scale) {
    return unit + (maxUnit - unit) * (scale / 100);
}

function validateData()
{
    const aX = document.getElementById('xA-value').value;
    const aY = document.getElementById('yA-value').value;
    const bX = document.getElementById('xB-value').value;
    const bY = document.getElementById('yB-value').value;
    const cX = document.getElementById('xC-value').value;
    const cY = document.getElementById('yC-value').value;


    if (isNaN(aX) || aX.trim() == "")
    {
        alert("Enter a valid x coordinate of point A");
        return false;
    }
    if (isNaN(aY) || aY.trim() == "")
    {
        alert("Enter a valid y coordinate of point A");
        return false;
    }
    if (isNaN(bX) || bX.trim() == "")
    {
        alert("Enter a valid x coordinate of point B");
        return false;
    }
    if (isNaN(bY) || bY.trim() == "")
    {
        alert("Enter a valid y coordinate of point B");
        return false;
    }
    if (isNaN(cX) || cX.trim() == "")
    {
        alert("Enter a valid x coordinate of point C");
        return false;
    }
    if (isNaN(cY) || cY.trim() == "")
    {
        alert("Enter a valid y coordinate of point C");
        return false;
    }
    return true;
}

function f(x)
{
    let a = parseFloat(document.getElementById('a-value').value);
    let b = parseFloat(document.getElementById('b-value').value);
    return a * x + b;
}

function drawCoordinates(scale) {
    const step = unit + (maxUnit - unit) * ((scale) / (100 - 1));
    const { maxOffsetX, maxOffsetY } = updateMaxOffset(scale);

    // Adjust offset to ensure canvas dragging is within limits
    offsetX = Math.max(-maxOffsetX, Math.min(maxOffsetX, offsetX)); // Змініть межі обмеження
    offsetY = Math.max(-maxOffsetY, Math.min(maxOffsetY, offsetY)); // Змініть межі обмеження

    ctx.clearRect(0, 0, width, height); // Clear the canvas
    ctx.fillStyle = "white";

    // Fill the entire canvas with white
    ctx.fillRect(0, 0, width, height);

    ctx.save(); // Save the current state
    ctx.translate(offsetX, offsetY); // Translate the canvas based on the offset

    // Draw the grid
    ctx.strokeStyle = '#a0a0a0';
    ctx.lineWidth = 1;

    for (let x = -maxOffsetX; x < width + maxOffsetX; x += step) {
        ctx.beginPath();
        ctx.moveTo(x, -maxOffsetY);
        ctx.lineTo(x, height + maxOffsetY);
        ctx.stroke();
    }

    for (let y = -maxOffsetY; y < height + maxOffsetY; y += step) {
        ctx.beginPath();
        ctx.moveTo(-maxOffsetX, y);
        ctx.lineTo(width + maxOffsetX, y);
        ctx.stroke();
    }

    for (let x = -maxOffsetX; x < width + maxOffsetX; x += step) {
        ctx.beginPath();
        ctx.moveTo(x, -maxOffsetY);
        ctx.lineTo(x, height + maxOffsetY);
        ctx.stroke();
    }

    // Draw the axes
    ctx.strokeStyle = '#000000';
    ctx.lineWidth = 2;
    ctx.beginPath();
    ctx.moveTo(-maxOffsetX, 0);
    ctx.lineTo(width + maxOffsetX, 0);
    ctx.moveTo(0, -maxOffsetY);
    ctx.lineTo(0, height + maxOffsetY);
    ctx.stroke();

    // Draw the labels
    ctx.fillStyle = '#000000';
    ctx.font = '10px Arial';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';

    for (let x = -maxOffsetX; x < width + maxOffsetX; x += step) {
        let valueX = Math.round(x / step);
        ctx.fillText(valueX.toString(), x, 10);
    }

    for (let y = -maxOffsetY; y < height + maxOffsetY; y += step) {
        let valueY = Math.round(y / step);
        ctx.fillText((-valueY).toString(), -10, y);
    }

    ctx.restore(); // Restore the state

    // Draw the axis labels
    ctx.font = 'bold 15px Arial';
    // Draw the axis labels
    ctx.font = 'bold 15px Arial';
    ctx.fillStyle = "black";
    const xAxisLabelX = width - 10;
    const yAxisLabelY = 10;

    // Ensure the labels are always visible on the screen
    ctx.fillText('X', xAxisLabelX, offsetY < 20 ? 20 : offsetY + 5);
    ctx.fillText('Y', offsetX < 20 ? 20 : offsetX, yAxisLabelY);


    let unitScale = calculateUnitScale(scale)
    if (trianglePoints.length){
        drawTriangle(...trianglePoints, unitScale);
    }
    if (tempTrianglePoints.length){
        drawTriangle(...tempTrianglePoints, unitScale, "red");
    }
}

function drawTriangle(pointA, pointB, pointC, unitScale, color="blue") {
    parallelogram = [pointA, pointB, pointC];

    // Перерахуємо координати з урахуванням масштабу
    const scaledA = { x: pointA.x * unitScale, y: pointA.y * unitScale };
    const scaledB = { x: pointB.x * unitScale, y: pointB.y * unitScale };
    const scaledC = { x: pointC.x * unitScale, y: pointC.y * unitScale };

    // Почнемо малювання
    ctx.beginPath();
    ctx.moveTo(scaledA.x + offsetX, -scaledA.y + offsetY);
    ctx.lineTo(scaledB.x + offsetX, -scaledB.y + offsetY);
    ctx.lineTo(scaledC.x + offsetX, -scaledC.y + offsetY);
    ctx.closePath();

    // Стилізація ліній паралелограма
    ctx.strokeStyle = color;
    ctx.lineWidth = 2;
    ctx.stroke();

    // Заповнення паралелограма кольором
    if (color === "blue"){
        ctx.fillStyle = 'rgba(0, 0, 255, 0.5)';

    }else if (color === "red"){
        ctx.fillStyle = 'rgba(219,69,69,0.5)';
    }
    ctx.fill();

    // Додавання підписів до точок
    const labelOffset = 5;
    ctx.fillStyle = 'black';
    ctx.font = '14px Arial';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    if (color === "blue"){
        ctx.fillText('A', scaledA.x + offsetX, -scaledA.y + offsetY - labelOffset);
        ctx.fillText('B', scaledB.x + offsetX, -scaledB.y + offsetY - labelOffset);
        ctx.fillText('C', scaledC.x + offsetX, -scaledC.y + offsetY - labelOffset);
    }else if (color === "red") {
        ctx.fillText("A1", scaledA.x + offsetX, -scaledA.y + offsetY - labelOffset);
        ctx.fillText("B1", scaledB.x + offsetX, -scaledB.y + offsetY - labelOffset);
        ctx.fillText("C1", scaledC.x + offsetX, -scaledC.y + offsetY - labelOffset);
    }
}


function addMatrices(matrixA, matrixB) {
    // Initialize the result matrix with zeros
    const result = Array(matrixA.length).fill(Array(matrixA[0].length).fill(0));

    // Perform matrix addition
    for (let i = 0; i < matrixA.length; i++) {
        for (let j = 0; j < matrixA[i].length; j++) {
            result[i][j] = matrixA[i][j] + matrixB[i][j];
        }
    }
    return result;
}

function multiplyMatrices(a, b) {
    const rowsA = a.length;
    const colsA = a[0].length;
    const rowsB = b.length;
    const colsB = b[0].length;
    const result = new Array(rowsA).fill(0).map(row => new Array(colsB).fill(0));

    if (colsA !== rowsB) {
        throw new Error('Matrix multiplication not possible: column/row mismatch');
    }

    for (let i = 0; i < rowsA; i++) {
        for (let j = 0; j < colsB; j++) {
            for (let k = 0; k < colsA; k++) {
                result[i][j] += a[i][k] * b[k][j];
            }
        }
    }
    return result;
}

function subtractMatrices(matrixA, matrixB) {
    // Check if both matrices have the same dimensions
    if (matrixA.length !== matrixB.length || matrixA[0].length !== matrixB[0].length) {
        throw new Error('Matrices are not the same size');
    }

    // Create a new matrix to store the result of the subtraction
    var result = new Array(matrixA.length);

    for (var i = 0; i < matrixA.length; i++) {
        result[i] = new Array(matrixA[i].length);

        for (var j = 0; j < matrixA[i].length; j++) {
            // Subtract each element of matrixB from matrixA
            result[i][j] = matrixA[i][j] - matrixB[i][j];
        }
    }
    return result;
}

function affineTransform(pointA, pointB, pointC, angle, scaleX, scaleY) {
    const rad = Math.PI / 180 * angle;

    // Матриця обертання (розширена до 3x3)
    const rotate = [
        [Math.cos(rad), Math.sin(rad), 0],
        [-Math.sin(rad), Math.cos(rad), 0],
        [0, 0, 1]
    ];

    // Матриця масштабування (розширена до 3x3)
    const scale = [
        [scaleX, 0, 0],
        [0, scaleY, 0],
        [0, 0, 1]
    ];

    const pointAMatrix = [[pointA.x, pointA.y, 1]]

    // Матриця перенесення на початок координат
    let translatedMatrixToZero =[
        [1, 0, 0],
        [0, 1, 0],
        [-pointA.x, -pointA.y, 1]
    ]

    // Матриця перенесення на точку A
    let translatedMatrixPointA = [
        [1, 0, 0],
        [0, 1, 0],
        [pointA.x, pointA.y, 1]
    ]

    function transform(point) {
        let pointMatrix = [[point.x, point.y, 1]]
        // let translated = subtractMatrices(pointMatrix, pointAMatrix)

        console.log(translatedMatrixToZero)

        let translated = multiplyMatrices(pointMatrix, translatedMatrixToZero)
        let rotatedMatrix = multiplyMatrices(translated, rotate);
        let scaledMatrix = multiplyMatrices(rotatedMatrix, scale);

        // let translated_back = addMatrices(scaledMatrix, pointAMatrix)
        let translated_back =  multiplyMatrices(scaledMatrix, translatedMatrixPointA)

        // Виконання матричного множення
        return {
            x: translated_back[0][0],
            y: translated_back[0][1]
        };
    }

    return [transform(pointA), transform(pointB), transform(pointC)];
}

function mirrorParallelogram(p, a, b) {
    const movementData = [];
    const step1Movement = [];
    const step2Movement = [];
    const step3Movement = [];
    const step4Movement = [];
    const step5Movement = [];
    const theta = Math.atan(a);
    const translationMatrix = [[0, -b / 50]];
    const translationMatrixBack = [[0, b / 50]];
    const mirrorToXMatrix = [
        [1, 0],
        [0, -1]
    ];
    const rotationMatrix = [
        [Math.cos(theta / 50), -Math.sin(theta / 50)],
        [Math.sin(theta / 50), Math.cos(theta / 50)]
    ];
    const inverseRotationMatrix = [
        [Math.cos(theta / 50), Math.sin(theta / 50)],
        [-Math.sin(theta / 50), Math.cos(theta / 50)]
    ];

    let step1, step2, step3, step4, step5;

    for (let i = 0; i < p.length; ++i)
    {
        step1 = [[p[i].x, p[i].y]];
        for (let j = 0; j < 50; ++j)
        {
            step1 = addMatrices(step1, translationMatrix);
            step1Movement.push({x: step1[0][0], y: step1[0][1]});
        }

        step2 = step1;

        for (let j = 0; j < 50; ++j)
        {
            step2 = multiplyMatrices(inverseRotationMatrix, step2);
            step2Movement.push({x: step2[0][0], y: step2[0][1]});
        }

        step3 = step2;

        step3 = multiplyMatrices(mirrorToXMatrix, step3);
        step3Movement.push({x: step3[0][0], y: step3[0][1]});

        step4 = step3;

        for (let j = 0; j < 50; ++j)
        {
            step4 = multiplyMatrices(rotationMatrix, step4);
            step4Movement.push({x: step4[0][0], y: step4[0][1]});
        }

        step5 = step4;

        for (let j = 0; j < 50; ++j)
        {
            step5 = addMatrices(step5, translationMatrixBack);
            step5Movement.push({x: step5[0][0], y: step5[0][1]});
        }
    }
    movementData.push(step1Movement);
    movementData.push(step2Movement);
    movementData.push(step3Movement);
    movementData.push(step4Movement);
    movementData.push(step5Movement);
    return movementData;
}


canvas.addEventListener('mousedown', function(event) {
    dragStart = {
        x: event.offsetX - offsetX,
        y: event.offsetY - offsetY
    };
});

canvas.addEventListener('mousemove', function(event) {
    if (dragStart) {
        offsetX = event.offsetX - dragStart.x;
        offsetY = event.offsetY - dragStart.y;
        if (!isStartClicked){
            drawCoordinates(scale);
        }
    }
});

canvas.addEventListener('mouseup', function(event) {
    dragStart = null;
});

canvas.addEventListener('mouseleave', function(event) {
    dragStart = null;
});

// Initialize the grid
drawCoordinates(scale);

function buildTriangle() {
    isBuildClicked = true;
    if (validateData())
    {
        trianglePoints = extractTriangleStartCoordinate()
        tempTrianglePoints = []
        drawCoordinates(scale);
    }
    document.getElementById('startMovement').onclick = startMovingTriangle;
}

function extractTriangleStartCoordinate(){
    return [{x: parseFloat(document.getElementById('xA-value').value),
        y: parseFloat(document.getElementById('yA-value').value)},

        {x: parseFloat(document.getElementById('xB-value').value),
            y: parseFloat(document.getElementById('yB-value').value)},

        {x: parseFloat(document.getElementById('xC-value').value),
            y: parseFloat(document.getElementById('yC-value').value)}]
}

function startMovingTriangle()
{
    if (isStartClicked){
        return;
    }
    if(!isBuildClicked) {
        alert("First you need to build triangle");
    } else {
        isStartClicked = true;
        isStopClicked = false;

        if (validateData() && isBuildClicked)
        {
            let angle = parseInt(document.getElementById('angle').value);
            let scaleXFrom = parseFloat(document.getElementById('scaleXFrom').value);
            let scaleYFrom = parseFloat(document.getElementById('scaleYFrom').value);
            // let scaleXTo = parseInt(document.getElementById('scaleXTo').value);
            // let scaleYTo = parseInt(document.getElementById('scaleYTo').value);
            // let useScaleTo = document.getElementById('useScaleTo').checked;

            let scaleX = scaleXFrom;
            let scaleY = scaleYFrom;

            var start_cors = extractTriangleStartCoordinate()
            let temp_angle = 0;
            let pause = 100;
            let functionArray = []
            while (temp_angle <= angle){
                let cors = affineTransform(...start_cors, temp_angle, scaleX, scaleY)

                function tempF(c) {
                    tempTrianglePoints = c
                    drawCoordinates(scale);
                }
                functionArray.push(()=> tempF(cors))

                if (temp_angle + 10 > angle){
                    let cors = affineTransform(...start_cors, angle, scaleX, scaleY)
                    functionArray.push(()=> tempF(cors))
                    break
                }else{
                    temp_angle += 10
                }
            }
            function functionCycle(i){
                setTimeout(() => {
                    if (!isStartClicked){
                        return
                    }
                    let func = functionArray[i]
                    if (func){
                        func()
                        functionCycle(i+1)
                    }else{
                        isStartClicked = false
                    }
                }, pause)
            }
            functionCycle(0)
        }
    }
}


document.getElementById('xA-value').addEventListener("input", function() {
    isStartClicked = false;
});
document.getElementById('yA-value').addEventListener("input", function() {
    isStartClicked = false;
});
document.getElementById('xB-value').addEventListener("input", function() {
    isStartClicked = false;
});
document.getElementById('yB-value').addEventListener("input", function() {
    isStartClicked = false;
});
document.getElementById('xC-value').addEventListener("input", function() {
    isStartClicked = false;
});
document.getElementById('yC-value').addEventListener("input", function() {
    isStartClicked = false;
});

document.getElementById('useScaleTo').addEventListener("change", function() {
    const block = document.getElementById("blockScaleTo");
    if (this.checked) {
        block.style.display = 'block'
    } else {
        block.style.display = 'none'
    };
});

function exportMovingImage()
{
    const dataURL = document.getElementById("moving-img").toDataURL("image/png");
    var a = document.createElement('a');
    a.href = dataURL;
    a.download = 'movingImage.jpeg';
    a.click();
}

document.getElementById("scaleImage").addEventListener('input', function() {
    scale = parseFloat(this.value);
    drawCoordinates(scale);
});

function stopSimulation()
{
    if(!isBuildClicked) {
        alert("First you need to build paralelogram and line");
    } else {
        isStartClicked = false;
        isStopClicked = true;
    }
}